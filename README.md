# rest-adapter

Modify the output of an existing rest service and return filtered result by url parameters.

## Start the micro-service

`mvn spring-boot:run`

## Call the service status

Status returns `OK` if service is healthy.

1. Start the micro-servcie
2. Open web browser
3. http://localhost:8080/status should return "OK"

## Get all films

Call `http://localhost:8080/film` from web browser to get all films.

## Filter films by search word 

Add `/search` after film in the url, then the films will be filtered by that word.
Eg.: `http://localhost:8080/film/2011` wil return the films that was released in 2011 or other film attributes contains 2011 