//Defining angular module..
var app=angular.module('Films',[]);

//Adding Filmscontroller to app module
app.controller('FilmsController',function($scope,$http){
  //Defining Films Array to Collect Films
  $scope.films=[
    {
      title:'Film1 ',
	  authour: 'Author1',
      place:'SanFrancisco',
    },
    {
      title:'Film2 ',
	  authour: 'Author1',
      place:'SanFrancisco',
    },
    {
      title:'Film3',
	  authour: 'Author1',
      place:'SanFrancisco',
    }
  ];

  //Angular Method for Adding film
  $scope.addFilm=function(filmTitle,filmAuthor,filmPages){
    $scope.film.push({title:filmTitle,author:filmAuthor,pages:filmPages});
  };

  //Angular Method for Deleting Film
  $scope.delete=function(key){
    //deleting film by it's index.
    $scope.film.splice(key,1);
  }

  //Angular Method for Updating Film
  $scope.update=function(title,author,places,key){
    $scope.films[key].title=title;
    $scope.films[key].author=author;
    $scope.films[key].places=places;
    $scope.edit=false;
  }
});
